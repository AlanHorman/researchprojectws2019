#!/usr/bin/env python3
#
# Copyright (c) 2019 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

import os, lgsvl

sim = lgsvl.Simulator(os.environ.get("SIMULATOR_HOST", "127.0.0.1"), 8181)
if sim.current_scene == "BorregasAve":
  sim.reset()
else:
  sim.load("BorregasAve")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]
forward = lgsvl.utils.transform_to_forward(spawns[0])
#state.transform.position += 5 * forward# 5m forwards
state.transform.position += 70 * forward
a = sim.add_agent("Lincoln2017MKZ (Apollo 5.0)", lgsvl.AgentType.EGO, state)

print("Current time = ", sim.current_time)
print("Current frame = ", sim.current_frame)
print()
print("Position of Vehicle: ", state.transform.position)
print("Forward: ", forward)

input("Press Enter to start driving")

# VehicleControl objects can only be applied to EGO vehicles
# You can set the steering (-1 ... 1), throttle and braking (0 ... 1), handbrake and reverse (bool)
c = lgsvl.VehicleControl()
c.throttle = 0.3
c.steering = -1.0
# a True in apply_control means the control will be continuously applied ("sticky"). False means the control will be applied for 1 frame
a.apply_control(c, True)

depth_cam_counter = 1
main_cam_counter = 1
sem_cam_counter = 1
sensors = a.get_sensors()

def saveFrames(n):
    
    n += 1
    
    global depth_cam_counter
    global main_cam_counter
    global sem_cam_counter
    
    for i in range(n):
        sim.run(time_limit=6, time_scale=1)

        for s in sensors:
            if s.name == "Depth Camera":
                if (depth_cam_counter < n):
                    s.save("./images/depth-camera" + str(depth_cam_counter) + ".jpg", quality=75)
                    depth_cam_counter += 1
            elif s.name == "Main Camera":
                if (main_cam_counter < n):
                    s.save("./images/main-camera" + str(main_cam_counter) + ".jpg", quality=75)
                    main_cam_counter += 1
            elif s.name == "Semantic Camera":
                if (sem_cam_counter < n):
                    s.save("./images/sem-camera" + str(sem_cam_counter) + ".jpg", quality=75)
                    sem_cam_counter += 1  
                    
                    
                
saveFrames(4)	
print("Finish!")