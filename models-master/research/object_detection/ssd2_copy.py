import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import cv2
import datetime
import random
import json

from collections import defaultdict
from io import StringIO
# from matplotlib import pyplot as plt

from PIL import Image

#os.chdir( '.' )

# ## Env setup

sys.path.append("..")

# ## Object detection imports
# Here are the imports from the object detection module.

#from object_detection.utils import label_map_util
#from object_detection.utils import string_int_label_map_pb2
#from utils import visualization_utils as vis_util

from object_detection.utils import label_map_util

from object_detection.utils import visualization_utils as vis_util

# # Model preparation

# ## Variables
#
# Any model exported using the `export_inference_graph.py` tool can be loaded here simply by changing `PATH_TO_CKPT` to point to a new .pb file.
#
# By default we use an "SSD with Mobilenet" model here. See the [detection model zoo](https://github.com/tensorflow/models/blob/master/object_detection/g3doc/detection_model_zoo.md) for a list of other models that can be run out-of-the-box with varying speeds and accuracies.

# In[ ]:

# What model to download.
MODEL_NAME = 'ssd_mobilenet_v1_coco_2018_01_28'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
#PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map_nl.pbtxt')
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

# ## Load a (frozen) Tensorflow model into memory.

# In[ ]:

print('.', end='')

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

print('.')

# ## Loading label map
# Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine

# In[ ]:


label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# ## Helper code

# In[ ]:


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


# # Detection

# In[ ]:


# For the sake of simplicity we will use only 2 images:
# image1.jpg
# image2.jpg
# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
PATH_TO_TEST_IMAGES_DIR = 'test_images'
# TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, 3) ]

DIR = 'test_images'
img_count = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]) + 1

TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, img_count) ]

# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)


# In[ ]:


#print(TEST_IMAGE_PATHS)

json_obj = {
	"images": []
}
	  
with detection_graph.as_default():
  with tf.Session(graph=detection_graph) as sess:
    for image_path in TEST_IMAGE_PATHS:
      image = Image.open(image_path)
      # the array based representation of the image will be used later in order to prepare the
      # result image with boxes and labels on it.
      image_np = load_image_into_numpy_array(image)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = detection_graph.get_tensor_by_name('detection_scores:0')
      classes = detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      (boxes, scores, classes, num_detections) = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
      # Visualization of the results of a detection.
      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=8)

      #print("image " + image_path.split('.')[0] + '_labeled.jpg')

      keywords = ""
      i = 0
      #print(scores)
      print("")	  
      print(image_path.split('\\')[1])	
      print("Image size (Height, Width, Channels):", image_np.shape)
      print("-------------------------------")	  
      while (i < len(np.squeeze(scores))):
        #  print('The count is:', i)
         currentScore = np.squeeze(scores)[i]
         if currentScore >= 0.50:
             currentClasses = np.squeeze(classes).astype(np.int32)[i]
             #currentClasses = np.squeeze(classes).astype(np.int32)
             #classNames = []			
             #for c in currentClasses:
             #             classNames.append(category_index[c]["name"])
             print("Object:", currentClasses)
             print("Score:", currentScore)
             print("Box (ymin, xmin, ymax, xmax):", np.squeeze(boxes)[i])
             print("Classes:", np.squeeze(classes)[i])
             print("Number Detection:", np.squeeze(num_detections))
             print("")


             #keywords += category_index[currentClasses]["name"] + ", "
             _boxes = np.squeeze(boxes)[i]
             #print(category_index[currentClasses])
             curr_obj = {
				  "ImageName" : image_path.split('\\')[1],
				  "ImageShape": image_np.shape,
				  "ClassNames": str(category_index[currentClasses]["name"]),
                  "Classes": int(np.squeeze(classes)[i]),
                  "Boxes": [float(_boxes[0]), float(_boxes[1]), float(_boxes[2]), float(_boxes[3])], 
                  "Scores": float(currentScore), 
                  "NumberDetections": int(np.squeeze(num_detections))	
			 }
			 
             json_obj["images"].append(curr_obj)
			 
             im_rgb = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
             #filename = "test" + str(random.randint(0, 1000000000)) + "json"
             cv2.imwrite("./output/output_" + image_path.split('\\')[1], im_rgb);
             cv2.imshow("video output", im_rgb)
			 
			 
             #f = open("./output/info.txt", "a")
             #f.write(category_index[currentClasses]["name"] + "" + filename + ": Classes: " + str(classes) + ", Boxes: " + str(boxes) + ", Scores: " + str(scores) + ", Number Detections: " + str(num_detections))
             #f.close()
             k = cv2.waitKey(10) & 0xFF
			 
             if k == 27:
                break
				
             #print("")

            #  print(category_index[currentClasses]["name"])
            #  print(category_index[currentClasses])
         i = i + 1

      #if len(keywords) >= 3:
          #print(keywords[:-2])

      #print(len(np.squeeze(scores)))
      #for i in range(len(np.squeeze(scores))):
      #    if np.squeeze(scores)[i] > 0.65:
      #        print(classNames[i])
      #        print(np.squeeze(scores)[i])
      #        print(np.squeeze(boxes)[i])
		  
      with open('data.json', 'w', encoding='utf-8') as f:
            json.dump(json_obj, f, ensure_ascii=False, indent=4)
			
print(json_obj)	  
		  

      #cv2.destroyAllWindows()
