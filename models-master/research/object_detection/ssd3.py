#!/usr/bin/env python3
#
# Copyright (c) 2019 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

import numpy as np
import os
import sys
import tensorflow as tf
import cv2
import json
import lgsvl
import time


# from matplotlib import pyplot as plt

from PIL import Image


# ## Load a (frozen) Tensorflow model into memory.

#os.chdir( '.' )      


########### Here starts the main program!

sim = lgsvl.Simulator(os.environ.get("SIMULATOR_HOST", "127.0.0.1"), 8181)

if sim.current_scene == "BorregasAve":
  sim.reset()
else:
  sim.load("BorregasAve")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]
forward = lgsvl.utils.transform_to_forward(spawns[0])
#state.transform.position += 5 * forward# 5m forwards
state.transform.position += 70 * forward
a = sim.add_agent("Lincoln2017MKZ (Apollo 5.0)", lgsvl.AgentType.EGO, state)

print("Current time = ", sim.current_time)
print("Current frame = ", sim.current_frame)
print()
print("Position of Vehicle: ", state.transform.position)
print("Forward: ", forward)

input("Press Enter to start driving")

# VehicleControl objects can only be applied to EGO vehicles
# You can set the steering (-1 ... 1), throttle and braking (0 ... 1), handbrake and reverse (bool)
c = lgsvl.VehicleControl()
c.throttle = 0.3
c.steering = -1.0
# a True in apply_control means the control will be continuously applied ("sticky"). False means the control will be applied for 1 frame
a.apply_control(c, True)

sensors = a.get_sensors()

def saveFrames(n):
        
    for i in range(n):
        sim.run(time_limit=6, time_scale=3)

        # The relative path for saving the images starts in the folder of the simulator
        for s in sensors:
            if s.name == "Depth Camera":
                if (i < n):
                    s.save("C:/Users/Ramón Wilhelm/Desktop/researchprojectws2019/models-master/research/object_detection/simulator_images/depth_images/depthImage_" + str(i  + 1) + ".jpg", quality=75)
            elif s.name == "Main Camera":
                if (i < n):
                    s.save("C:/Users/Ramón Wilhelm/Desktop/researchprojectws2019/models-master/research/object_detection/test_images/image_" + str(i  + 1) + ".jpg", quality=75)
            elif s.name == "Semantic Camera":
                if (i < n):
                    s.save("C:/Users/Ramón Wilhelm/Desktop/researchprojectws2019/models-master/research/object_detection/simulator_images/semantic_images/semImage_" + str(i  + 1) + ".jpg", quality=75)

saveFrames(40)	
print("SSD for color images is starting now...")


# In[ ]:

# ## Env setup

sys.path.append("..")

# ## Object detection imports
# Here are the imports from the object detection module.

#from object_detection.utils import label_map_util
#from object_detection.utils import string_int_label_map_pb2
#from utils import visualization_utils as vis_util

from object_detection.utils import label_map_util

from object_detection.utils import visualization_utils as vis_util

# # Model preparation

# ## Variables
#
# Any model exported using the `export_inference_graph.py` tool can be loaded here simply by changing `PATH_TO_CKPT` to point to a new .pb file.
#
# By default we use an "SSD with Mobilenet" model here. See the [detection model zoo](https://github.com/tensorflow/models/blob/master/object_detection/g3doc/detection_model_zoo.md) for a list of other models that can be run out-of-the-box with varying speeds and accuracies.

# In[ ]:

# What model to download.
MODEL_NAME = 'ssd_mobilenet_v1_coco_2018_01_28'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
#PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map_nl.pbtxt')
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

NUM_CLASSES = 90

# ## Loading label map
# Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine

# In[ ]:


label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# ## Helper code

# In[ ]:


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


# # Detection

# In[ ]:


# For the sake of simplicity we will use only 2 images:
# image1.jpg
# image2.jpg
# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
PATH_TO_TEST_IMAGES_DIR = 'test_images'

#Path to the semantic test images
PATH_TO_TEST_SEM_IMAGES_DIR = './simulator_images/semantic_images'

#Path to the depth test images
PATH_TO_TEST_DEPTH_IMAGES_DIR = './simulator_images/depth_images'

# TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, 3) ]

DIR = 'test_images'

# DIR to the semantic images
DIR_SEM = './simulator_images/semantic_images'

# DIR to the depth images
DIR_DEPTH = './simulator_images/depth_images'

img_count = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]) + 1
sem_img_count = len([name for name in os.listdir(DIR_SEM) if os.path.isfile(os.path.join(DIR_SEM, name))]) + 1
depth_img_count = len([name for name in os.listdir(DIR_DEPTH) if os.path.isfile(os.path.join(DIR_DEPTH, name))]) + 1

TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image_{}.jpg'.format(i)) for i in range(1, img_count) ]
TEST_SEM_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_SEM_IMAGES_DIR, 'semImage_{}.jpg'.format(i)) for i in range(1, sem_img_count) ]
TEST_DEPTH_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_DEPTH_IMAGES_DIR, 'depthImage_{}.jpg'.format(i)) for i in range(1, depth_img_count) ]

# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)


# In[ ]:



json_obj = {
	"images": []
}

json_obj_sem = {
	"images": []
}

json_obj_depth = {
	"images": []
}


def performSSD(TEST_IMAGE_PATHS, output_path, json_dic, json_path):	 
  with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
      for image_path in TEST_IMAGE_PATHS:
        image = Image.open(image_path)
        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        image_np = load_image_into_numpy_array(image)
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = detection_graph.get_tensor_by_name('detection_scores:0')
        classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')
        # Actual detection.
        (boxes, scores, classes, num_detections) = sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})
        # Visualization of the results of a detection.
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=8)

        i = 0
        print("")	  
        print(image_path.split('\\')[1])	
        print("Image size (Height, Width, Channels):", image_np.shape)
        print("-------------------------------")	  
        
        temp_name = image_path.split('\\')[1]
        temp_name = temp_name.split('.')[0]
        imgNumber = temp_name.split('_')[1]
        
        curr_obj_1 = {
                #"ImageName" : image_path.split('\\')[1],
                "Image" : int(imgNumber),
                "ImageShape": image_np.shape,
                "NumberDetections": int(np.squeeze(num_detections)),
                "Classes" : []
		}
        
        while (i < len(np.squeeze(scores))):
           currentScore = np.squeeze(scores)[i]
           if currentScore >= 0.50:
               currentClasses = np.squeeze(classes).astype(np.int32)[i]

               print("Object:", currentClasses)
               print("Score:", currentScore)
               print("Box (ymin, xmin, ymax, xmax):", np.squeeze(boxes)[i])
               print("Classes:", np.squeeze(classes)[i])
               print("Number Detection:", np.squeeze(num_detections))
               print("")


               _boxes = np.squeeze(boxes)[i]
               curr_obj_2 = {			    
				    "ClassName": str(category_index[currentClasses]["name"]),
                    "Class": int(np.squeeze(classes)[i]),
                    "Box": [float(_boxes[0]), float(_boxes[1]), float(_boxes[2]), float(_boxes[3])], 
                    "Score": float(currentScore)			   
                }
                              

               curr_obj_1["Classes"].append(curr_obj_2)
               json_dic["images"].append(curr_obj_1)
               
               im_rgb = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
               cv2.imwrite(output_path + image_path.split('\\')[1], im_rgb);				

           i += 1
          
        result_json = []    
        
        for j in json_dic["images"]:
            if j not in result_json:
                result_json.append(j)
                
        json_dic["images"] = result_json
		  
        with open(json_path, 'w', encoding='utf-8') as f:
              json.dump(json_dic, f, ensure_ascii=False, indent=4)
      

print('.', end='')

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

print('.')


                        
performSSD(TEST_IMAGE_PATHS, "./output/output_", json_obj,'output_json/data.json')
print("SSD for semantic images is starting now...")
performSSD(TEST_SEM_IMAGE_PATHS, "./output_semantic/output_", json_obj_sem,'output_json/data_sem.json')
print("SSD for depth images is starting now...")
performSSD(TEST_DEPTH_IMAGE_PATHS, "./output_depth/output_", json_obj_depth,'output_json/data_depth.json')

#perform_sem_SSD()