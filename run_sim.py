#!/usr/bin/env python3
#
# Copyright (c) 2019 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

import os, lgsvl, time, threading

sim = lgsvl.Simulator(os.environ.get("SIMULATOR_HOST", "127.0.0.1"), 8181)
if sim.current_scene == "BorregasAve":
  sim.reset()
else:
  sim.load("BorregasAve")

spawns = sim.get_spawn()

state = lgsvl.AgentState()
state.transform = spawns[0]
forward = lgsvl.utils.transform_to_forward(spawns[0])
#state.transform.position += 5 * forward# 5m forwards
state.transform.position += 70 * forward
a = sim.add_agent("Lincoln2017MKZ (Apollo 5.0)", lgsvl.AgentType.EGO, state)

print("Current time = ", sim.current_time)
print("Current frame = ", sim.current_frame)
print()
print("Position of Vehicle: ", state.transform.position)
print("Forward: ", forward)

input("Press Enter to start driving")

# VehicleControl objects can only be applied to EGO vehicles
# You can set the steering (-1 ... 1), throttle and braking (0 ... 1), handbrake and reverse (bool)
c = lgsvl.VehicleControl()
c.throttle = 0.3
c.steering = -1.0
# a True in apply_control means the control will be continuously applied ("sticky"). False means the control will be applied for 1 frame
a.apply_control(c, True)

depth_cam_counter = 0
main_cam_counter = 0
sem_cam_counter = 0
sensors = a.get_sensors()

image = None

def saveFrames(n):
	print(threading.currentThread().getName())
	thread = threading.Timer(6.0, saveFrames, {n: n})
	thread.start()
	
	global depth_cam_counter
	global main_cam_counter
	global sem_cam_counter
	
	global image 
	
	if threading.currentThread().getName() != "MainThread":
		for s in sensors:
			if s.name == "Depth Camera":
				if (depth_cam_counter <= n-1):
					image = s.save("./images/depth-camera" + str(depth_cam_counter) + ".jpg", quality=75)
					depth_cam_counter += 1
				else:
					time.sleep(5)
					thread.cancel()
				

			elif s.name == "Main Camera":
				if (main_cam_counter <= n-1):
					s.save("./images/main-camera" + str(main_cam_counter) + ".jpg", quality=75)
					main_cam_counter += 1
				else:
					time.sleep(5)
					thread.cancel()
				

			elif s.name == "Semantic Camera":
				if (sem_cam_counter <= n-1):
					s.save("./images/sem-camera" + str(sem_cam_counter) + ".jpg", quality=75)
					sem_cam_counter += 1
				else:
					time.sleep(5)
					thread.cancel()
					
		sim.run(time_limit=6, time_scale=3)
	

saveFrames(3)	
sim.run(time_limit=6, time_scale=3)